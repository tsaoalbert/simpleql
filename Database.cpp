/*
CREATE cs445class 3 string string bool emotion person Y/N
INSERT INTO cs445class 8 ROWS
*/

#include <iostream>
#include <fstream>


#include "Database.h"

using namespace std;

// cstr
Database::Database ( const string& name) : mname(name)
{
}

// dstr
Database::~Database ( ) 
{
}

// SORT TABLE fruits BY COLUMN color
void Database::executeSortTable ( const vector<string>& tokens) {
  const string& tableName = tokens[2];
  auto& t1 = findTable ( tableName ); // find out the table object to do the rest work.

  const string& colName  = tokens[5];
  t1->sortRow ( t1->getColumnIndex(colName)  );
}

// TODO: implement command PRINT ROW FROM
// Example:  PRINT ROW FROM fruits 2 name weight WHERE color = red

void Database::executePrintRow ( const vector<string>& tokens) {
  const string& tableName = tokens[3];
  auto& t1 = findTable ( tableName );

  size_t n = tokens.size();
  const string& colName  = tokens[ n -3];
  t1->printRow ( tokens );
}

void Database::executeInsertRowInto ( ifstream& fin, const vector<string>& tokens) {
  const string& tableName = tokens[3];
  auto& t1 = findTable ( tableName );
  int n = stoi(tokens[4]);
  for (size_t i=0;  i < n ; ++i ) {
    string line ;
    getline( fin , line) ;
    // cout << line << endl;
    t1->insertRow ( line );
  }
}



void Database::deleteColumnFrom ( const vector<string>& tokens) { 
  const string& tableName = tokens[3];
  auto& t1 = findTable ( tableName );

  for (size_t i = 4; i < tokens.size(); ++i ) {
    const string& columnName = tokens[i];
    // cout << columnName << endl;
    t1->deleteColumn ( columnName ); // delete column by name
  }
}

std::unique_ptr<Table>&  
Database::createTable  (
  const std::string& name, size_t numRows, 
  const std::vector <std::string > & types, 
  const std::vector <std::string > & columnNames
) {

  Table* t1 = new Table (name, numRows,types, columnNames);
  return createTable  ( t1 );
}

std::unique_ptr<Table>& Database::createTable  ( Table* table) 
{
  mtables.emplace_back ( table );
  return mtables.back();
}

const unique_ptr<Table>& Database::createTable( const vector<string>& tokens)            
{
  const string& tableName = tokens[2];
  int n = stoi(tokens[3]);
 
  vector<string> colNames ;
  vector<string> types ;
 
  for (size_t i = 0 ; i < n; ++i  ) {
    types.push_back ( tokens[4+i] ) ;
    colNames.push_back ( tokens[4+n+i] ) ;
  }
  return createTable  (tableName, 0,types, colNames);
}




  void Database::displayAll ( ) const 
  {
    std::cout << std::endl;
    std::cout << "Database: " << mname << std::endl;
    for (auto& t: mtables )  {
      cout << t.get() << endl;
      // t->displayAll(); 
    }
    std::cout << std::endl;
  }

/*
      JOIN TABLE myFRuits AND unitPrices WHERE
            name = species AND PRINT 3 name 1 color  2  price/oz 3
*/
  void Database::join ( const vector<string>& tokens ) {
    const string& tname1 = tokens[2];
    const string& tname2 = tokens[4];

    const string& cname1 = tokens[6];
    const string& cname2 = tokens[8];

    auto& t1 = findTable ( tname1 );
    auto& t2 = findTable ( tname2 );
    t1->join ( cname1, cname2, t2 );
}
