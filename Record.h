#pragma once

#include <string>
#include <vector>
#include <memory>

class Record {

private:
  // why vector contains pointers to the cells of the base class
  vector< unique_ptr<SpreadsheetCell> > m_cells; // member data

public: 

  // getter
  const vector< unique_ptr<SpreadsheetCell> > & getCells() 
  const {
    return m_cells;
  }


  // getter
  const unique_ptr<SpreadsheetCell>& getCell (size_t i ) const {
    return m_cells[i];
  }

  // mutator
  void addCell (SpreadsheetCell* q) {
    m_cells.emplace_back (q) ;
  } 

  // mutator
  void deleteCell (size_t i ) {
    m_cells.erase ( m_cells.begin()+i);
  } 
};
