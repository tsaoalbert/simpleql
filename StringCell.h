#pragma once

#include <typeinfo>
#include <iostream>
#include "Cell.h"

using namespace std;

/* TODO: implement the Class Template Specialization, described on page 334, 
          chapter 11, Textbook  
*/


/*
When the template specialization is used, the original template must be 
visible too. Including it here ensures that it will always be visible 
when this specialization is visible.

template <>
class Cell <string>: public SpreadsheetCell

This syntax tells the compiler that this class is a string specialization of the 
SpreadsheetCell class. Suppose that you didn’t use that syntax and just 
tried to write this:
class Cell : public SpreadsheetCell

The compiler wouldn’t let you do that because there is already a class named 
Cell  (the original class template). Only by specializing it can you reuse the 
name. 

The main benefit of specializations is that they can be invisible to the user. 
When a user creates a Cell of ints or double, the compiler generates code from 
the original Cell template. When the user creates a Cell of string, 
the compiler uses the string specialization. This can all be “behind the scenes.
*/

