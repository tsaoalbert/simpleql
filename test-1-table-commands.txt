CREATE TABLE fruits 5 string int string  float  bool name weight color unitPrice expired
INSERT ROW INTO fruits 7 ROWS
apple   1         red       12.5         false
kiwi    3         pink      9         false
apple   2         red       12.5         false
banana  1         yellow    4.5         false
gava    2         green     4.5         false
grape   1         purple    5.5         true
orange  2         orange    5.5         false

SORT TABLE fruits BY COLUMN unitPrice
PRINT TABLE fruits 

SORT TABLE fruits BY COLUMN weight
PRINT TABLE fruits 

#
PRINT ROW FROM fruits 3 name weight unitPrice WHERE unitPrice >= 5

