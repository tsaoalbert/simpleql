#pragma once

#include <typeinfo>
#include <iostream>
#include "SpreadsheetCell.h"
using namespace std;

/* Class Cell is the template class derived from the base class SpreadsheetCell
   The member data in Class cell can be int, string, double, char, bool, ...
*/

template <class T>
class Cell : public SpreadsheetCell
{
public:
	Cell() = delete; // explicitly disable default cstr
	Cell( const T & v ) : mValue(v) {}  ; // cstr 

	inline T get () const {return mValue;} // improve performance by remving func call 
  
  // The following override the pure virtual function in the base class
	virtual ostream& display ( ostream& out = cout) const override ;

private:
	T mValue; //  T can be any data type
};

template <class T>
ostream& Cell<T>::display ( ostream& out ) const {
  out << mValue ;
  return out ;
}
