/*
CREATE cs445class 3 string string bool emotion person Y/N
INSERT INTO cs445class 8 ROWS
*/


#include <sstream>
#include <iostream>
#include <iomanip>
#include <vector>
#include <memory>
#include <algorithm>

#include "Cell.h"
#include "StringCell.h"
#include "Record.h"
#include "Table.h"

using namespace std;

size_t Table::mcolumnWidth = 10;

// display is a overriden function to enable polymorphism 
ostream &operator<< (ostream &out, const SpreadsheetCell *r) {
  out << boolalpha ; // print out boolean value as True/False instead of 0/1
  return r->display( out );
}

ostream &operator<< (ostream &out, const unique_ptr<SpreadsheetCell> &r) {
  out << r.get ();
  return out;
}

// for script command: SORT ROW 
template<class T>
T dget ( const SpreadsheetCell* c) {
  const auto p1 = dynamic_cast< const Cell< T > *> ( c );
  return p1->get ();
}

template<class T>
T dget ( const unique_ptr<SpreadsheetCell>& c) {
  return dget<T> (c.get ());
}


string toString ( DataType t ) {
  vector<string> msg = {"string", "int", "double", "bool", "bool", "float" };
  return  msg [static_cast<size_t> (t)];
}

template<class T> 
vector<size_t> Table::selectRow ( size_t i, const string& op, const T& key ) const {
  vector<size_t> rows;
  size_t j = 0 ;
  for ( const auto& row: mdata  ) {
    const auto& x = dget<T> ( row.getCell(i) );
    if (  
      (op == "="  && x == key ) || 
      (op == "<=" && x <= key ) || 
      (op == ">=" && x >= key ) 
    ) {
      rows.push_back(j);
    }
    ++j;
  }
  return move (rows);
}
// Example:  PRINT ROW FROM fruits 2 name weight WHERE color = red
// collect the records specified in the commands
vector<size_t> Table::selectRow ( const vector<string>& tokens) {
  size_t n = tokens.size();
  const string& colName  = tokens[ n -3];

  const size_t i = mcolumn2Index[colName];

  const string& key   = tokens[ n -1];
  const string& op   = tokens[ n -2]; // >, =, < 
  vector<size_t> rows;
  switch  (mtypes[i]) { // data type of column i
    case DataType::DOUBLE :
      rows = selectRow <double>(i, op, stod(key));
      break;
    case  DataType::BOOLEAN :
      rows = selectRow <bool>(i, op, key=="true");
      break;
    case  DataType::STRING :
      rows = selectRow <string >(i, op, key);
      break;
    case  DataType::CHAR :
      rows = selectRow < char  >(i, op, key[0]);
      break;
    case  DataType::INTEGER :
      rows = selectRow <int>(i, op, stoi(key));
      break;
    case  DataType::FLOAT :
      rows = selectRow <float>(i, op, stof(key)); // string to float
      break;
  }
  return move ( rows );
}
#define COLUMN_NAME   -1
#define DASH_LINE   -2
// print selected fields of row i
void Table::printOneRow ( int i, const vector<string>& tokens) {
  int m = stoi ( tokens[4] );
  for ( size_t j= 0; j < m; ++j ) { // for each selected column
    const string& colName  = tokens[ 5+j ];
    if ( mcolumn2Index.find ( colName) != cend( mcolumn2Index) ) {
      size_t k = mcolumn2Index[colName]; // map column name to index
      cout << setw(mcolumnWidth) ;
      if ( i ==  COLUMN_NAME ) {
        cout << mcolumnNames[k] ;
      } else if ( i== DASH_LINE ) {
        string s (mcolumnWidth-2, '-') ;
        cout << setw(mcolumnWidth) <<  s;
      } else { //
        cout << mdata[i].getCell(k) ;
      }
    }
  }
  cout << endl;
}
/* TODO: implement the following command PRINT ROW FROM */
void Table::printRow ( const vector<string>& tokens) {
  auto rows = selectRow ( tokens); // select the specified rows
  printOneRow ( COLUMN_NAME, tokens) ; // print out the selected rows
  printOneRow ( DASH_LINE, tokens) ;
  for (auto& i: rows ) { // print out each selected rows i
    printOneRow ( i, tokens) ;
  }
}


class MyCompare{
public:
  MyCompare (size_t i , DataType t): m_i(i), m_type(t) {};

  // overloaded function operator ()

  bool operator()(const Record&  a, const Record&  b) {
    const auto& pa = a.getCell(m_i) ;
    const auto& pb = b.getCell(m_i) ;
    switch  (m_type) {
    case DataType::DOUBLE :
      return dget< double > (pa) < dget< double >(pb);
      // TODO: fill in the missing codes.             
      break;
    case  DataType::BOOLEAN :
      return dget< bool > (pa) < dget< bool >(pb);
      // TODO: fill in the missing codes.             
      break;
    case  DataType::STRING :
      return dget< string > (pa) < dget< string >(pb);
      // TODO: fill in the missing codes.             
      break;
    case  DataType::CHAR :
      return dget< char > (pa) < dget< char >(pb);
      // TODO: fill in the missing codes.             
      break;
    case  DataType::INTEGER :
      return dget< int > (pa) < dget< int >(pb);
      // TODO: fill in the missing codes.             
      break;
    case  DataType::FLOAT :
      return dget< float > (pa) < dget< float >(pb);
      // TODO: fill in the missing codes.             
      break;
    default:
      return true ;
    }
    return false ;
  }

  size_t m_i; // index to the column for comparison
  DataType m_type ; // the data type of the selected column
};
void Table::sortRow ( const size_t i ) { // TODO: fill in the missing codes
  MyCompare cmp (i, mtypes[i] );
  sort ( begin(mdata), end(mdata), cmp );
}



void Table::appendDataType ( const string & s ) {
  if ( s=="string") {
    appendDataType (DataType::STRING);
  } else if ( s=="double" ) {
    appendDataType (DataType::DOUBLE);
  } else if ( s=="int" ) {
    appendDataType (DataType::INTEGER);
  } else if ( s=="bool" ) {
    appendDataType (DataType::BOOLEAN);
  } else if ( s=="char" ) {
    appendDataType (DataType::CHAR);
  } else if ( s=="float" ) {
    appendDataType (DataType::FLOAT);
  } else {
    cerr << "ERROR: unknown data type " << s << "!!!" << endl;
    exit(0);
  }
}

void Table::appendDataType (DataType t) {
  mtypes.push_back ( t );
}

using myType = const unique_ptr<SpreadsheetCell>&  ;
bool compare ( myType a, myType b, const DataType & m_type)
 {
    switch  (m_type) {
    case DataType::DOUBLE :
      {
        auto p1 = dynamic_cast< Cell<double> *> ( a.get() ); 
        auto p2 = dynamic_cast< Cell<double> *> ( b.get()    ); 
        return p1->get() < p2->get();
      }
      break;
    case  DataType::BOOLEAN :   
      {
        auto p1 = dynamic_cast< Cell< bool > *> ( a.get()    ); 
        auto p2 = dynamic_cast< Cell< bool > *> ( b.get()    ); 
        return p1->get() < p2->get();
      }
      break;
    case  DataType::STRING :   
      {
        auto p1 = dynamic_cast< Cell<string> *> ( a.get()    ); 
        auto p2 = dynamic_cast< Cell<string> *> ( b.get()    ); 
        return p1->get() < p2->get();
      }
      break;
    case  DataType::CHAR :   
      {
        auto p1 = dynamic_cast< Cell<char> *> ( a.get()    ); 
        auto p2 = dynamic_cast< Cell<char> *> ( b.get()    ); 
        return p1->get() < p2->get();
      }
      break;
    case  DataType::FLOAT :   
      {
        auto p1 = dynamic_cast< Cell< float > *> ( a.get()    ); 
        auto p2 = dynamic_cast< Cell< float > *> ( b.get()    ); 
        return p1->get() < p2->get();
      }
      break;
    case  DataType::INTEGER :   
      {
        auto p1 = dynamic_cast< Cell<int> *> ( a.get()    ); 
        auto p2 = dynamic_cast< Cell<int> *> ( b.get()    ); 
        return p1->get() < p2->get();
      }
      break;
    }
 }

void Table::join ( const string& cname1, const string& cname2, const unique_ptr<Table> & t2 ) {
  size_t i =  mcolumn2Index[cname1] ;
  size_t j =  t2->mcolumn2Index[cname2] ;
  cout << "JOIN Table " << mname << ", column=\" " << cname1 << "\"" ;
  cout << " with Table " << t2->mname << ", column=\" " << cname2 << "\"" << endl;
  for ( const auto& r1: mdata ) {
    for ( const auto& r2: t2->mdata ) {
      bool ok =  compare ( r1.getCell(i), r2.getCell(j), mtypes[i] );
      if (ok ) {
        for ( const auto& c: r1.getCells() ) { cout << setw(mcolumnWidth) << c ; } 
        for ( const auto& c: r2.getCells() ) { cout << setw(mcolumnWidth) << c ; } 
        cout << endl;
      }  
    }
  }
}

ostream& Table::displayAll ( ostream& out ) const {
  out << endl;
  out << "Table: " << mname << endl;
  for ( auto& s: mcolumnNames ) {
    out << setw(mcolumnWidth) << s;
  } 
  out << endl;
  for ( auto& t: mtypes ) {
    size_t len = (mcolumnWidth>2)? (mcolumnWidth-2): 2 ;
    string s (len, '-') ;
    out << setw(mcolumnWidth) <<  s;
  } 
  out << endl;
  for ( const auto& r: mdata ) {
    size_t i = 0 ;
    for ( const auto& c: r.getCells() ) {
      auto& type = mtypes[i];
      out << setw(mcolumnWidth) << c ;
      ++i;
    }
    out << endl;
  }
  return out ;
}

// xxx
SpreadsheetCell * createCell (const string& s, DataType type) {

  // cell is instantiated as different derived type 

  SpreadsheetCell *q = nullptr; // base class pointer
  switch (type ) {
    case DataType::STRING:  
     // q = new Cell<string>  ( s, 15 ); 
     q = new Cell<string>  ( s ); // cell is instantiated as different cell type
     break;
    case DataType::FLOAT:  
      q = new Cell< float > ( stof( s) );
       break;
    case DataType::INTEGER:  
      q = new Cell<int> ( stoi( s) );
       break;
    case DataType::DOUBLE:  
      q = new Cell<double> ( stod(s) );
       break;
    case DataType::BOOLEAN:
       q = new Cell<bool> ( s=="true" );
       break;
    case DataType::CHAR:
       q = new Cell<char> ( s[0] );
       break;
    default:
      cerr << "ERROR: unknown data type." << endl;
  }
  return q;
}

Table::Table (const string& name, size_t numRows, 
  const vector<string> &types, const vector<string> &columnNames ) 
  : mname(name)
{
  for (size_t i=0; i < types.size(); ++i ) {
    insertColumn (types[i], columnNames[i] );
  }
  // cout << "Create Table " << name << endl;
}

bool Table::insertRow ( const string& line ) {
  istringstream iss( line );
  vector<string> v{istream_iterator<string>{iss}, istream_iterator<string>{}};
  return insertRow ( v );
}

void Table::insertColumn ( const string &type, const string& colName ) {
  if ( mcolumn2Index.find ( colName) != mcolumn2Index.end() ) {
    // column exists
    cout << "Cannot add column " << colName << " in Table " << mname << endl;
    return ;
  }
  size_t n = mcolumnNames.size();  
  appendDataType ( type );
  mcolumnNames.push_back( colName ) ; 
  mcolumn2Index[colName] = n;
  for ( auto& row: mdata) {
   string s ;
   SpreadsheetCell *q = createCell (s, mtypes.back() ) ;
   row.addCell ( q );
  }
}

// detect and delete duplicate column
void Table::deleteColumn ( const string &colName ) {
  if ( mcolumn2Index.find ( colName) == mcolumn2Index.end() ) {
    // detect non-exitent column
    cout << "Cannot find column " << colName << " in Table " << mname << endl;
  } else {
    cout << "Delete column " << colName << " from Table " << mname << endl;
    size_t i =  mcolumn2Index[colName] ;
    cout << "i=" << i << endl;
    mcolumn2Index.erase ( mcolumn2Index.find(colName) );
    deleteColumn (  i );
  }  
}

// insert a new record
bool Table::insertRow (const vector<string>&v ) {

  size_t i = 0; 
   Record r;
  for (const auto& s: v ) {
    SpreadsheetCell *q = createCell (s, mtypes[i] ) ;
    r.addCell( q );
    ++i;
  }
  mdata.push_back ( move(r) );
  bool ok = ( i == mtypes.size() ) ;
  if ( !ok ) { 
    cerr << "ERROR: i = " << i << endl;
    cerr << "ERROR: mtypes.size() = " << mtypes.size() << endl;
    cerr << "ERROR: insertRow fails!!!" << endl;
    exit(0);
  }
  return ok ;
}

// delete row (record) i
void Table::deleteRow (size_t i  ) {
  cout << "delete row " <<  i << endl;
  mdata.erase (mdata.begin()+i);
}

// delete column (property) i
void Table::deleteColumn (size_t i  ) {
  cout << *( mcolumnNames.begin() + i ) << endl ;
  // cout << *( mtypes.begin() + i ) << endl ;

  mtypes.erase ( mtypes.begin() + i ) ;
  mcolumnNames.erase ( mcolumnNames.begin() + i ) ;
  size_t j = 0 ;
  j = 0 ;
  for ( auto& c: mdata) {
    c.deleteCell( i);
    j++;
  }
}

