#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <iterator>
#include <vector>
#include <memory>

#include "StringCell.h"
#include "Table.h"
#include "Database.h"

using namespace std;

/*
TODO:  The given starter programs supports only 5 data types: 
        int, double, string, char and bool. 
        You are required to add one more data type: float. 

*/

void executeCommand ( Database& db, const vector<string>& tokens,
   ifstream& fin ) {
  string key = tokens[0]+tokens[1] ;
  if ( key == "CREATETABLE" ) {
    db.createTable ( tokens) ;
  } else if ( key == "PRINTTABLE" ) {
    const string& tname1 = tokens[2];
    auto& t1 = db.findTable ( tname1 );
    cout << t1 << endl; 
  } else if ( key == "PRINTDATABASE" ) {
    db.displayAll ();
  } else if ( key == "INSERTROW" ) {
    db.executeInsertRowInto ( fin, tokens) ;
  } else if ( key == "DELETECOLUMN" ) {
    db.deleteColumnFrom ( tokens) ;
  } else if ( key == "JOINTABLE" ) {
    db.join ( tokens );
  } else if ( key == "SORTTABLE" ) { // TODO: to be finished
    db.executeSortTable ( tokens );
  } else if ( key == "PRINTROW" ) {
    db.executePrintRow ( tokens) ; // TODO: implement PRINT ROW
/*
*/
  } else {
    cout << "ERROR: unrecognized command syntax: " << endl;
  }
}
void executeCommandScript ( Database& db, const string& fn) {
  ifstream fin (fn);
  string line ;
  cout << left << endl;
  while ( getline( fin , line) ) // get a line from the fin
  {
    istringstream iss( line );
    vector<string> tokens {istream_iterator<string>{iss}, istream_iterator<string>{}};
    size_t n = tokens.size();
    if ( n >0 && tokens[0] == "QUIT" ) { break; }
    if ( n >= 2 && tokens[0][0] != '#' )  { // if not a comment line beginning wtih #
      cout << ">>> " << line << endl; // echo the comand line
      executeCommand ( db, tokens, fin ) ;
    }
  }
  fin.close();

} 

int main()
{
  Database db ("db1" );
  string fn = "test-1-table-commands.txt";

  executeCommandScript (db, fn);

  // unitTestTable ();

}

