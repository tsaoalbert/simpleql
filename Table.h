#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <memory>

#include "SpreadsheetCell.h"
#include "Record.h"

using namespace std;

// all the data type for each cell
enum class DataType {STRING, INTEGER, DOUBLE, BOOLEAN, CHAR, FLOAT };

void unitTestTable () ;

class Table {
  friend ostream& operator<<(ostream& out, const Table* t )  {
    return t->displayAll(out);
  }
  friend ostream& operator<<(ostream& out, const unique_ptr<Table>& t )  {
    return t->displayAll(out);
  }
  // member data
private: 
  vector <Record> mdata; // a list of records
  vector <DataType> mtypes; // a list of data type 
  vector <string> mcolumnNames;  // a list of column title
  // to speed up the mapping from column name to column index
  map<string, size_t> mcolumn2Index; // map from column name to column index 
  string mname;                     // table name 
  static size_t mcolumnWidth ;      // for print out format

private: // helper function 
  void appendDataType ( const string  & s ) ;
  void appendDataType (DataType t) ;

  bool insertRow (const vector<string>&v ) ;
  void deleteColumn (size_t column ) ;

  ostream& displayAll ( ostream& out = cout) const ;
  template<class T> 
  vector<size_t> selectRow ( size_t i, const string& op, const T& key ) const; 
  vector<size_t> selectRow ( const vector<string>& tokens); 
  void printOneRow ( int i, const vector<string>& tokens) ;

public:
  void printRow ( const vector<string>& tokens); 

  // cstr
  Table () = default ;
  inline const string& getName () const { return mname; } ;
  Table (const string& , size_t, const vector<string> &, const vector<string> & ) ;
  
  inline size_t getColumnIndex ( const string& s ) const {
    const auto& it = mcolumn2Index.find (s );
    if ( it==mcolumn2Index.end() ) return 0;
    return it->second;
  }
  bool insertRow ( const string& line ) ;
  void deleteRow (size_t row ) ;

  void join ( const string& cname1, const string& cname2, const unique_ptr<Table> & t2 ) ;

  void insertColumn ( const string &type, const string& name ) ;
  void deleteColumn ( const string & name  ) ;

  void sortRow ( const size_t i ) ;

};


