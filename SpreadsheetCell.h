#pragma once

#include <string>
#include <iostream>
using namespace std;

// SpreadsheetCell is a base cell in the table

// Cell of different data type will be derived from it  
class SpreadsheetCell
{
  public:

  // 1. the keyword "virtual" is necessary for polymorphism
  // 2. pure virtual function can  force the derived class to 
  // implement the funciton 

	virtual ostream& display ( ostream& out = cout) const = 0; 

  // data will be defined in the derived class.
};
